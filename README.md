## API Init -- Cryptographie, SSH, OpenPGP

Ce dépôt contient les sources LaTeX utilisées pour générer un support de présentation pour l'API Init.

Une chaîne d'intégration permet de construire automatiquement le PDF à partir du fichier .tex présent à la racine.

Après construction (uniquement sur la branche `master`), le document final est disponible [à cette adresse](https://uploads.picasoft.net/api/openpgp.pdf).

Le nom du fichier à compiler ainsi que le chemin d'upload sont définis dans le fichier [.gitlab-ci.yml](./gitlab-ci.yml).
